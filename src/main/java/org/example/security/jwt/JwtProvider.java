package org.example.security.jwt;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class JwtProvider {

    @Value("${aws.identityPoolUrl}")
    private String identityPoolUrl;

    @Autowired
    ConfigurableJWTProcessor configurableJWTProcessor;

    private static final String USERNAME_FIELD = "username";
    private static final String BEARER = "Bearer ";
    private static final String AUTHORIZATION = "Authorization";

    public Authentication authenticate(HttpServletRequest request) throws Exception {
        String token = request.getHeader(AUTHORIZATION);
        if(Objects.nonNull(token)){
            JWTClaimsSet claims = configurableJWTProcessor.process(getToken(token),null);
            validateToken(claims);
            String username= getUsername(claims);
            if(Objects.nonNull(username)){
                //TODO set Roles
                List<GrantedAuthority> authorities= List.of(new SimpleGrantedAuthority("ROLE_USER"));
                User user=new User(username,"",authorities);
                return new JwtAuthenticator(authorities,user,claims);
            }
        }
        return null;
    }

    private String getUsername(JWTClaimsSet claims) {
        return claims.getClaim(USERNAME_FIELD).toString();
    }

    private void validateToken(JWTClaimsSet claims) throws Exception {
        if (!claims.getIssuer().equals(identityPoolUrl))
            throw new Exception("JWT not valid");
    }

    private String getToken(String token){
        //Se valida que aveces viene la palabra Bearer antes del token
        return token.startsWith(BEARER) ? token.substring(BEARER.length()) : token;
    }

}
