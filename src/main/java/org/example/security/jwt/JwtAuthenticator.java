package org.example.security.jwt;

import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Objects;

public class JwtAuthenticator extends AbstractAuthenticationToken {

    private Object principal;//Usuario que se logea
    private JWTClaimsSet claims;//Objeto que contiene todos los valores del token

    public JwtAuthenticator(Collection<? extends GrantedAuthority> authorities, Object principal, JWTClaimsSet claims) {
        super(authorities);
        this.principal = principal;
        this.claims = claims;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
