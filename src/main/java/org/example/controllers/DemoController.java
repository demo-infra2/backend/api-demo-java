package org.example.controllers;

import org.example.dto.ResponseDTO;
import org.example.services.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private DemoService service;

    @GetMapping
    public ResponseEntity<ResponseDTO> getMensaje() {
        try {
            return ResponseEntity.ok(service.serviceDemo());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/react")
    public ResponseEntity<ResponseDTO> getMensajeReact() {
        try {
            return ResponseEntity.ok(service.serviceDemoReact());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }


}
