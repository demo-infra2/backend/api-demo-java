package org.example.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class ResponseDTO {
    private Integer code;
    private String message;

}
