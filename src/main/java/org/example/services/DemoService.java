package org.example.services;

import org.example.dto.ResponseDTO;
import org.springframework.stereotype.Service;

@Service
public class DemoService {


    public ResponseDTO serviceDemo(){
        ResponseDTO response=new ResponseDTO();
        response.setCode(1);
        response.setMessage("Menú Inicial");
        return response;
    }

    public ResponseDTO serviceDemoReact(){
        ResponseDTO response=new ResponseDTO();
        response.setCode(1);
        response.setMessage("Mensaje Correcto--OK");
        return response;
    }

}
