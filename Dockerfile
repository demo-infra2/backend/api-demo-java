# Usa una imagen base de Java
FROM openjdk:11-jre-slim

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el archivo JAR de tu aplicación al contenedor
#COPY target/*.jar app.jar
COPY build/libs/*.jar /api.jar

#ENTRYPOINT ["java","-jar","/app.jar"]

CMD ["-jar", "/api.jar"]
ENTRYPOINT ["java"]
EXPOSE 8082